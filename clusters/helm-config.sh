
#!/bin/bash
# Input ENV variables
export KUBECONFIG=$2
export RANCHER_HOSTNAME=$1
echo $1
echo $2
# Helm setup rbac
kubectl --kubeconfig ${KUBECONFIG} -n kube-system create serviceaccount tiller
kubectl --kubeconfig ${KUBECONFIG} create clusterrolebinding tiller \
  --clusterrole=cluster-admin \
  --serviceaccount=kube-system:tiller
helm init --service-account tiller
kubectl --kubeconfig ${KUBECONFIG} -n kube-system  rollout status deploy/tiller-deploy
echo ..................10s..................
sleep 10
# Helm install cert-manager
kubectl --kubeconfig ${KUBECONFIG} apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml
kubectl --kubeconfig ${KUBECONFIG} create namespace cert-manager
echo ..................20s..................
sleep 20
kubectl --kubeconfig ${KUBECONFIG} get po --all-namespaces
#kubectl --kubeconfig ${KUBECONFIG} label namespace cert-manager certmanager.k8s.io/disable-validation=true
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install --name cert-manager --namespace cert-manager --version v0.12.0 jetstack/cert-manager
kubectl --kubeconfig ${KUBECONFIG} -n cert-manager rollout status deploy/cert-manager
echo ......................20s....................
sleep 20
kubectl --kubeconfig ${KUBECONFIG} get pods --namespace cert-manager
# Helm install Rancher
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm install rancher-latest/rancher   --name rancher   --namespace cattle-system  --set hostname=$1 --set ingress.tls.source=letsEncrypt --set letsEncrypt.email=alaahamdi@57@gmail.com
echo ......................20s....................

sleep 20
kubectl --kubeconfig ${KUBECONFIG} get po -n cattle-system