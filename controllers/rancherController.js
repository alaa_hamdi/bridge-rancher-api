const fs = require('fs');
const shelljs = require('shelljs');
const yaml = require('js-yaml');
const child_process = require('child_process');

module.exports.deployRancher = (req, res) => {
    var clusters = JSON.parse(fs.readFileSync('config/clusters.json').toString());
    if (clusters.length) {
        clusterByName(clusters, req.params.cluster_name, (selected_cluster) => {
            if (!selected_cluster) console.log(`ERROR: CLUSTER_NOT_FOUND, message: (${req.params.cluster_name}) is not found in the list of clusters :(`);
            else {
                console.log(selected_cluster);
                //prepare for rke up
                const CLUSTER_CONFIG_PATH = 'clusters/' + req.params.cluster_name + '/cluster.yaml';
                const CLUSTER_CONFIG_FOWLDER = 'clusters/' + req.params.cluster_name;
                shelljs.mkdir(CLUSTER_CONFIG_FOWLDER);
                if (shelljs.error()) {
                    console.log('CAUSE : ' + CLUSTER_CONFIG_FOWLDER + ' Folder already exists ...\nFIX: Make sure that you delete ' + CLUSTER_CONFIG_FOWLDER);
                }
                else {
                    shelljs.touch(CLUSTER_CONFIG_PATH);
                    // init cluster.yaml file
                    fs.writeFileSync(CLUSTER_CONFIG_PATH, yaml.safeDump({ nodes: [] }), 'utf8');
                    const RANCHER_HOST = selected_cluster.dns[0];
                    // At this stage, we have three types of proiders : azure, aws and virtualbox
                    // virtualbox is for the local clusters
                    if (selected_cluster.provider == 'azure') {
                        for (var i = 0; i < selected_cluster.node_number; i++) {
                            if (i == 0 || i < selected_cluster.master_node_number) {
                                addMaster(
                                    selected_cluster.dns[i],
                                    selected_cluster.privateIps[i],
                                    selected_cluster.provider,
                                    selected_cluster.username,
                                    '',
                                    CLUSTER_CONFIG_PATH
                                );
                            }
                            else if (i >= selected_cluster.master_node_number && i < selected_cluster.master_node_number + selected_cluster.worker_node_number) {
                                addWorker(
                                    selected_cluster.dns[i],
                                    selected_cluster.privateIps[i],
                                    selected_cluster.provider,
                                    selected_cluster.username,
                                    '',
                                    CLUSTER_CONFIG_PATH
                                );
                            }
                            else {
                                addEtcd(
                                    selected_cluster.dns[i],
                                    selected_cluster.privateIps[i],
                                    selected_cluster.provider,
                                    selected_cluster.username,
                                    '',
                                    CLUSTER_CONFIG_PATH
                                );
                            }

                        }//end for
                        shelljs.exec('pwd && cd ' + CLUSTER_CONFIG_FOWLDER + ' && rke up --config ./cluster.yaml --ssh-agent-auth --ignore-docker-version\
                    && export KUBECONFIG=kube_config_cluster.yaml\
                    && kubectl get nodes && sleep 10\
                    && sh ../helm-config.sh '+ RANCHER_HOST + ' ./kube_config_cluster.yaml')

                        //'sh ../helm-config.sh dns-infra-1.westus.cloudapp.azure.com ./kube_config_cluster.yaml'

                        // }//end else check folder

                    }//end if azure
                    if (selected_cluster.provider == 'aws') {
                        // For aws
                    }
                    if (selected_cluster.provider == 'virtualbox') {
                        for (var i = 0; i < selected_cluster.node_number; i++) {
                            if (i == 0 || i < selected_cluster.master_node_number) {
                                addMaster(
                                    '',
                                    selected_cluster.privateIps[i],
                                    selected_cluster.provider,
                                    selected_cluster.username,
                                    '',
                                    CLUSTER_CONFIG_PATH
                                );
                            }
                            else if (i >= selected_cluster.master_node_number && i < selected_cluster.master_node_number + selected_cluster.worker_node_number) {
                                addWorker(
                                    '',
                                    selected_cluster.privateIps[i],
                                    selected_cluster.provider,
                                    selected_cluster.username,
                                    '',
                                    CLUSTER_CONFIG_PATH
                                );
                            }
                            else {
                                addEtcd(
                                    '',
                                    selected_cluster.privateIps[i],
                                    selected_cluster.provider,
                                    selected_cluster.username,
                                    '',
                                    CLUSTER_CONFIG_PATH
                                );
                            }

                        }//end for
                        shelljs.exec('pwd && cd ' + CLUSTER_CONFIG_FOWLDER + ' && rke up --config ./cluster.yaml --ssh-agent-auth --ignore-docker-version\
                    && export KUBECONFIG=kube_config_cluster.yaml\
                    && kubectl get nodes && sleep 10\
                    && sh ../helm-config.sh '+ RANCHER_HOST + ' ./kube_config_cluster.yaml')

                        //'sh ../helm-config.sh dns-infra-1.westus.cloudapp.azure.com ./kube_config_cluster.yaml'

                    }
                }
            }

        });
    }
    else {
        console.log('NO_CLUSTER_IS_FOUND')
    }
}

function clusterByName(clusters, cluster_name, selected_cluster) {
    // const cluster = new Array
    var stop = false;
    clusters.forEach(element => {
        if (element.cluster_name == cluster_name) { stop = true; selected_cluster(element) }
        if (element == clusters[clusters.length - 1] && !stop) selected_cluster(null);
    });
}
// Add worker
function addWorker(dns = "", privateIp, provider, username, ssh_key_name = "", cluster_config_path) {
    // "worker"
    // For azure provider
    if (provider == 'azure') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: dns,
            internal_address: privateIp,
            user: username,
            role: ["worker"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    // For AWS provider
    if (provider == 'aws' && ssh_key_name != 'undefined') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: dns,
            internal_address: privateIp,
            user: username,
            ssh_key_path: ssh_key_name,
            role: ["controlplane", "worker", "etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    // virtualbox
    if (provider == 'virtualbox') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: privateIp,
            internal_address: privateIp,
            user: username,
            role: ["worker"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    else {
        // catch the Error
    }
}
// Add master
function addMaster(dns = "", privateIp, provider, username, ssh_key_name = "", cluster_config_path) {
    //"controlplane", "worker", "etcd"
    // VirtualBox
    if (provider == 'virtualbox') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        console.log(clusterContent);
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: privateIp,
            internal_address: privateIp,
            user: username,
            role: ["controlplane", "worker", "etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    // For azure provider
    if (provider == 'azure') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        console.log(clusterContent);
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: dns,
            internal_address: privateIp,
            user: username,
            role: ["controlplane", "worker", "etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    // For AWS provider
    if (provider == 'aws' && ssh_key_name != 'undefined') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: dns,
            internal_address: privateIp,
            user: username,
            ssh_key_path: ssh_key_name,
            role: ["controlplane", "worker", "etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    else {
        // catch the Error
    }
}
// Add Etcd
function addEtcd(dns = "", privateIp, provider, username, ssh_key_name = "", cluster_config_path) {
    // "etcd"
    //virtualbox
    if (provider == 'virtualbox') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: privateIp,
            internal_address: privateIp,
            user: username,
            role: ["etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    // For azure provider
    if (provider == 'azure') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: dns,
            internal_address: privateIp,
            user: username,
            role: ["etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    // For AWS provider
    if (provider == 'aws' && ssh_key_name != 'undefined') {
        let clusterContent = fs.readFileSync(cluster_config_path, 'utf8');
        let clusterYAML = yaml.safeLoad(clusterContent);
        clusterYAML.nodes.push({
            address: dns,
            internal_address: privateIp,
            user: username,
            ssh_key_path: ssh_key_name,
            role: ["etcd"]
        });
        // Add to file
        let yamlString = yaml.safeDump(clusterYAML);
        fs.writeFileSync(cluster_config_path, yamlString, 'utf8');
    }
    else {
        // catch the Error
    }
}