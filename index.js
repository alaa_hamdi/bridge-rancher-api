const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());

// Rancher route
app.use('/bridge-api/rancher',require('./routes/rancher.js'));


const PORT =process.env.PORT ||3000;
app.listen(PORT,console.log(`Server started on port ${PORT}`)).timeout=50000000;

module.exports=[]